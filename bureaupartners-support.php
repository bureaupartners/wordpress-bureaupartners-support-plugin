<?php
/*
	Plugin Name: BureauPartners B.V. - Support
	Plugin URI: https://bureau.partners
	Description: WordPress BureauPartners Support plugin voor live chat intergratie
	Author: BureauPartners B.V.
	Author URI: https://bureau.partners
	Version: 1.0.2
	Bitbucket Plugin URI: https://bitbucket.org/bureaupartners/wordpress-bureaupartners-support-plugin
	Bitbucket Branch: master
*/
	
function bureaupartners_support_add_livechat(){
	wp_enqueue_script( 'bureaupartners_support_add_livechat', plugins_url( 'intercom.php' , __FILE__ ));

}

add_action( 'admin_enqueue_scripts', 'bureaupartners_support_add_livechat' ); 

?>