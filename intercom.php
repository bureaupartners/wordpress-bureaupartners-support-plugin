<?php
	header('Content-Type: application/javascript');
	include_once('../../../wp-load.php');
	$current_user = wp_get_current_user();
?>
window.intercomSettings = {
  app_id: "ts5pvicg",
  domain: "<?php echo $_SERVER['SERVER_NAME']; ?> / WordPress Admin Plugin",
  email: "<?php echo $current_user->user_email; ?>",
  name: "<?php echo $current_user->display_name; ?>"
};
(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/ts5pvicg';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()